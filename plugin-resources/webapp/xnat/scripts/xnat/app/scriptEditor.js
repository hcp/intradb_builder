/**
 * functions for XNAT script editor
 * xnat-templates/screens/Scripts.vm
 */

XNAT.app.scriptEditor = {};
XNAT.app.scriptEditor.scriptIds = [];

$(function XNATscriptEditor(){

    var scriptEditor = XNAT.app.scriptEditor;

    // sample of output from REST query
    // that retrieves script info
    var sampleScriptJSON = {
        "content": "// script content goes here",
        "enabled": true,
        "id": 1,
        //"asProperties" : {
        //    "script" : "// script content goes here",
        //    "languageVersion" : "2.3.6",
        //    "language" : "groovy",
        //    "description" : "First script",
        //    "scriptId" : "script1"
        //},
        "languageVersion": "2.3.6",
        "language": "groovy",
        "created": 1421265306548,
        "description": "First script",
        "scriptId": "script1",
        "disabled": 0,
        "timestamp": 1421265306548
    };

    // return script url with common parts pre-defined
    function scriptURL( scriptId, params ){
        // 'params' //-- array of query string params ['format=json']
        params = params || [];
        params.push('XNAT_CSRF='+csrfToken);
        return serverRoot + '/data/automation/scripts/' + scriptId + '?' + params.join('&');
    }

    scriptEditor.loadScripts = function(){
        // TODO: replace with AJAX to load/reload only the scripts table
        window.location.reload();
    };

    function saveScript( scriptId, $dialog, editor_id ){

        var $scriptIdInput = $dialog.find('.script-id-input');

        if ( !scriptId ) {
            xmodal.message('Error: Empty Script ID', 'Please give your script an ID before saving.');
        }
        // check for valid script ID
        else if (!/^[a-zA-Z][a-zA-Z0-9_]*$/.test(scriptId)){
            xmodal.message({
                title: 'Error: Invalid Characters in Script ID',
                message: 'Script ID can only consist of letters, numbers, and the underscore "_" character.',
                action: function(){
                    setTimeout(function(){
                        $scriptIdInput.focus().select();
                    },10);
                }
            });
        }
        // check to see if script already exists
        else if ( scriptEditor.scriptIds.indexOf($scriptIdInput.val()) > -1 ) {
            // TODO: (maybe) get list via REST instead of when the page loads
            xmodal.message({
                title: 'Error: Script ID Already Exists',
                message: 'A script with that ID already exists. Please enter a new ID and try again.',
                action: function(){
                    setTimeout(function(){
                        $scriptIdInput.focus().select();
                    },10);
                }
            });
        }
        else {

            editor_id =
                editor_id ||
                scriptEditor.editor_id ||
                $dialog.find('.editor-content').attr('id');

            var data = {
                content: ace.edit(editor_id).getSession().getValue(),
                description: $dialog.find('.script-description').val()
            };

            jQuery.ajax({
                type: 'PUT',
                url: scriptURL(scriptId),
                cache: false,
                data: data,
                dataType: "json",
                success: function(){
                    xmodal.message('Success', 'Your script was successfully saved.', {
                        action: function(){
                            scriptEditor.loadScripts();
                        }
                    });
                    xmodal.close($dialog);
                },
                error: function( request, status, error ){
                    xmodal.message('Error', 'An error occurred: [' + status + '] ' + error);
                }
            });
        }
    }

    // open xmodal dialog for script editing
    function renderEditor( json ){

        json = json || {};

        var scriptId = json.scriptId || '';
        var lang = json.language || 'groovy';
        var time = json.timestamp || '';

        var opts = {};
        opts.width = 880;
        opts.height = 660;
        opts.scroll = false;
        opts.template = $('#script-editor-template');
        opts.title = 'XNAT Script Editor';
        opts.title += (lang) ? ' - ' + lang : '';
        opts.title += (json.languageVersion) ? ' ' + json.languageVersion : '';
        opts.enter = false; // prevents modal closing on 'enter' keypress
        opts.footerContent = '<span style="color:#555;">';
        if (time){
            opts.footerContent +=
            'last modified: ' + (new Date(time)).toString();
        }
        opts.footerContent += '</span>';
        opts.buttons = {
            save: {
                label: 'Save and Close',
                action: function( obj ){
                    scriptId =
                        obj.$modal.find('.scriptId').val() ||
                        obj.$modal.find('.script-id-input').val();
                    saveScript(scriptId, obj.$modal)
                },
                isDefault: true,
                close: false
            },
            close: {
                label: 'Cancel'
            }
        };
        opts.beforeShow = function( obj ){

            var $dialog = obj.$modal;

            $dialog.find('.id').val(json.id || '');
            $dialog.find('.scriptId').val(scriptId);
            $dialog.find('.language').val(lang);
            $dialog.find('.timestamp').val(time);
            $dialog.find('.script-description').val(json.description || '');

            if (scriptId){
                $dialog.find('.script-id-text').html(scriptId);
                $dialog.find('.script-id-input').remove();
                //$dialog.find('.script-id-input').val(scriptId);
            }

            var $editor = $dialog.find('.editor-content');
            $editor.html(json.content || '');
            $editor.attr('id', (scriptId || obj.id) + '-editor-content');

            scriptEditor.editor_id = $editor.attr('id');

            var editor = ace.edit(scriptEditor.editor_id);
            editor.setTheme("ace/theme/eclipse");
            editor.getSession().setMode("ace/mode/" + lang);

        };
        opts.afterShow = function(obj){
            if (!scriptId){
                obj.$modal.find('.script-id-input').focus().select();
            }
        };
        xmodal.open(opts);
    }

    scriptEditor.editScript = function( scriptId ){
        $.getJSON(scriptURL(scriptId, ['format=json']), function( json ){
            renderEditor(json);
        });
    };

    scriptEditor.duplicateScript = function( scriptId ){
        $.getJSON(scriptURL(scriptId, ['format=json']), function( json ){
            var data = {
                //scriptId: json.scriptId + '_copy',
                description: json.description,
                content: json.content,
                language: json.language,
                languageVersion: json.languageVersion,
                copyOf: json.scriptId
            };
            renderEditor(data);
        });
    };

    scriptEditor.addScript = function(){
        //window.location = url;
        renderEditor(null);
    };

    function doDelete( scriptId ){

        var successDialog = {
            title: 'Success',
            content: 'The script was successfully deleted.',
            action: function(){
                scriptEditor.loadScripts();
            }
        };

        jQuery.ajax({
            type: 'DELETE',
            url: scriptURL(scriptId),
            cache: false,
            success: function(){
                xmodal.message(successDialog);
            },
            error: function( request, status, error ){
                xmodal.message('Error', 'An error occurred: [' + status + '] ' + error);
            }
        });
    }

    scriptEditor.deleteScript = function( scriptId ){
        xmodal.confirm({
            width: 450,
            title: 'Delete Script?',
            content: 'Are you sure you want to delete the script <b>"' + scriptId + '"</b>?<br/><br/>Any events or triggers associated with this script will also be deleted.',
            okLabel: 'Delete',
            cancelLabel: 'Cancel',
            okAction: function(){
                doDelete(scriptId);
            },
            cancelAction: function(){
                //xmodal.message('Delete cancelled', 'The script was not deleted.');
            }
        });
    };

    // ???
    $('body').addClass('administer configuration no_left_bar');

    $('#scripts_table').on('click', '[data-action]', function(){
        var action = $(this).data('action');
        var script = $(this).closest('tr').data('script-id');
        scriptEditor[action](script);
    });

});
