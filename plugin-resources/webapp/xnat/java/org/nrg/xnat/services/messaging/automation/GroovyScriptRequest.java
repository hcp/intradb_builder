package org.nrg.xnat.services.messaging.automation;

import org.nrg.xdat.security.XDATUser;

import java.io.Serializable;

public class GroovyScriptRequest implements Serializable {
	private static final long serialVersionUID = 6828367474920729056L;
	
	private final String srcWorkflowId;
	private final XDATUser user;
	private final String scriptId;
	private final String scriptWorkflowId;
	private final String datatype;
	private final String externalid;
	private final String dataid;
	
	public GroovyScriptRequest(String workflow, XDATUser user, String scriptId, String scriptWorkflow, String datatype, String dataid, String externalid){
		this.srcWorkflowId=workflow;
		this.user=user;
		this.scriptId= scriptId;
		this.scriptWorkflowId=scriptWorkflow;
		this.datatype=datatype;
		this.dataid=dataid;
		this.externalid=externalid;
	}

	public String getSrcWorkflowId() {
		return srcWorkflowId;
	}

	public XDATUser getUser() {
		return user;
	}

	public String getScriptId() {
		return scriptId;
	}

	public String getScriptWorkflowId() {
		return scriptWorkflowId;
	}

	public String getExternalId() {
		return externalid;
	}

	public String getDataType() {
		return datatype;
	}

	public String getDataId() {
		return dataid;
	}
}
