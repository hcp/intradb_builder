package org.nrg.xnat.services.messaging.automation;

import org.nrg.automation.services.ScriptRunnerService;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xdat.XDAT;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xnat.utils.WorkflowUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class GroovyScriptRequestListener {

	@SuppressWarnings("unused")
	public void onRequest(final GroovyScriptRequest request) throws Exception {
		final PersistentWorkflowI wrk = WorkflowUtils.getUniqueWorkflow(request.getUser(), request.getScriptWorkflowId());
		wrk.setStatus(PersistentWorkflowUtils.IN_PROGRESS);
        WorkflowUtils.save(wrk, wrk.buildEvent());

		final Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("user", request.getUser());
		parameters.put("srcWorkflowId", request.getSrcWorkflowId());
		parameters.put("groovyWorkflowId", request.getScriptWorkflowId());
		parameters.put("dataId",request.getDataId());
		parameters.put("externalId", request.getExternalId());
		parameters.put("dataType", request.getDataType());
		parameters.put("groovyWorkflow", wrk);

		try {
			_service.runScript(_service.getScript(request.getScriptId()), parameters);

			if(PersistentWorkflowUtils.IN_PROGRESS.equals(wrk.getStatus())){
				WorkflowUtils.complete(wrk,wrk.buildEvent());
			}
		} catch (NrgServiceException e) {
			logger.error("",e);
			if(PersistentWorkflowUtils.IN_PROGRESS.equals(wrk.getStatus())){
				WorkflowUtils.fail(wrk,wrk.buildEvent());
			}
		}
	}

	private final ScriptRunnerService _service = XDAT.getContextService().getBean(ScriptRunnerService.class);
	private static final Logger logger = LoggerFactory.getLogger(GroovyScriptRequestListener.class);
}
